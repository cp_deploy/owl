(function($$){

    var form = $$.Form = $$.View.extend(function(){

        this._elements = {};

        this._forms = {};

        this._state = ''

    },{
        className : 'owl-ui-form',
        render : function(){},
        addElement : function(){},
        getElement : function(){},
        addForm : function(){},
        validate : function(){},
        changed : function(){},
        reset : function(){},
        submit : function(){},
        reload : function(){},
        setState : function(){},
        getState : function(){}
    });

    $$.Form.Events = {
        SUBMIT_SUCESS : 'owl.form.events.submit_sucess',
        SUBMIT_ERROR : 'owl.form.events.submit_error',
        CHANGE_STATE : 'owl.form.events.change_state'
    };

    $$.Form.State = {
        RENDERING : 'owl.form.state.rendering',
        READY : 'owl.form.state.ready',
        ERROR : 'owl.form.state.error',
        INVALID : 'owl.form.state.invalid',
        SUBMITTING : 'owl.form.state.submitting'
    };

    $$.Form.StateClass = {
        RENDERING : 'rendering',
        READY : 'ready',
        ERROR : 'error',
        INVALID : 'invalid',
        SUBMITTING : 'submitting'
    };

    $$.VariableStorage.createStorage($$.Form,{
        ValidationMethods : {}
    });

    $$.Form.ValidationMethods.setValues({
        'required' : function(elem){
            return elem.val() == '';
        }
    });

    $$.Form.BaseElement = $$.View.extend(function(options){
        this._parentForm = options.parentForm;

        this.validationMethods = $.extend($$.form.ValidationMethods.getValues(),this.validationMethods);
    },{
        render : function(){
            
        },
        validate : function(){
            
        },
        showError : function(){

        },
        hideError : function(){
            
        },
        val : function(val){
            
        }
    });

    //TODO bind element's event, create easy interface to access elements

}(Owl));