(function($$){


    // Backbone.sync
    // -------------

    // Map from CRUD to HTTP for our default `Backbone.sync` implementation.
    $$.REQUEST_METHODS = {
        'create': 'POST',
        'update': 'PUT',
        'delete': 'DELETE',
        'read':   'GET'
    };
        // Turn on `emulateHTTP` to support legacy HTTP servers. Setting this option
    // will fake `"PUT"` and `"DELETE"` requests via the `_method` parameter and
    // set a `X-Http-Method-Override` header.
    $$.emulateHTTP = false;

    // Turn on `emulateJSON` to support legacy servers that can't deal with direct
    // `application/json` requests ... will encode the body as
    // `application/x-www-form-urlencoded` instead and will send the model in a
    // form param named `model`.
    $$.emulateJSON = false;

    // Helper function to get a value from a Owl object as a property
    // or as a function.
    $$.getValue = function(object, prop) {
        if (!(object && object[prop])) return null;
        var val = object[prop];
        return val.isFunction() ? val() : val;
    };

    // Throw an error when a URL is needed, and none is supplied.
    $$.urlError = function() {
        throw new Error('A "url" property or function must be specified');
    };

    // Override this function to change the manner in which Backbone persists
    // models to the server. You will be passed the type of request, and the
    // model in question. By default, makes a RESTful Ajax request
    // to the model's `url()`. Some possible customizations could be:
    //
    // * Use `setTimeout` to batch rapid-fire updates into a single request.
    // * Send up the models as XML instead of JSON.
    // * Persist models via WebSockets instead of Ajax.
    //
    // Turn on `Backbone.emulateHTTP` in order to send `PUT` and `DELETE` requests
    // as `POST`, with a `_method` parameter containing the true HTTP method,
    // as well as all requests with the body as `application/x-www-form-urlencoded`
    // instead of `application/json` with the model in a param named `model`.
    // Useful when interfacing with server-side languages like **PHP** that make
    // it difficult to read the body of `PUT` requests.
    $$.sync = function(method, model, options) {
        var type = $$.REQUEST_METHODS[method];

        // Default options, unless specified.
        if (!options) options = {};

        // Default JSON-request options.
        var params = {type: type, dataType: 'json'};

        // Ensure that we have a URL.
        if (!options.url)
            params.url = $$.getValue(model, 'url') || $$.urlError();


        // Ensure that we have the appropriate request data.
        if (!options.data && model && (method == 'create' || method == 'update')) {
            params.contentType = 'application/json';
            params.data = JSON.stringify(model.toJSON());
        }

        // For older servers, emulate JSON by encoding the request into an HTML-form.
        if ($$.emulateJSON) {
            params.contentType = 'application/x-www-form-urlencoded';
            params.data = params.data ? {model: params.data} : {};
        }

        // For older servers, emulate HTTP by mimicking the HTTP method with `_method`
        // And an `X-HTTP-Method-Override` header.
        if ($$.emulateHTTP) {
            if (type === 'PUT' || type === 'DELETE') {
                if ($$.emulateJSON) params.data._method = type;
                params.type = 'POST';
                params.beforeSend = function(xhr) {
                    xhr.setRequestHeader('X-HTTP-Method-Override', type);
                };
            }
        }

        // Don't process data on a non-GET request.
        if (params.type !== 'GET' && !$$.emulateJSON)
            params.processData = false;

        // Make the request, allowing the user to override any Ajax options.
        return $.ajax(_.extend(params, options));
    };

    // Wrap an optional error callback with a fallback error event.
    $$.wrapError = function(onError, originalModel, options) {
        return function(model, resp) {
            resp = model === originalModel ? resp : model;
            if (onError) {
                onError(originalModel, resp, options);
            } else {
                originalModel.trigger('error', originalModel, resp, options);
            }
        };
    };

    /*total:100000000

    getters: {
        total: "getTotal"
    },
    getTotal: function() {
        return Money.format(this.attributes.total);
    }
    observables: ["name"]
    this._observables["name"] = new Observable(val);
    this.attributes["name"] = new Observable(val);


    set: function(name, value) {
        var attr = this.attributes[name];
        if (attr instanceof Observable) {
            this.attributes
            var val = this.getters[name].isFunction()?this.getters[]
        }
        //observable.set(this.getters)
    }*/


var root = this;

var ModelStorage = {};
    
var Model = $$.Model = $$.Event.extend(function(attrs, options){
    if (!attrs) attrs = {};
    
},{
    //current model state
    _state : null, //[new,saving,pending,destroyed]
    _idName : {},
    _attributes : {},
    _attributesState: {},
    _observables: {},
    getters: {},
    setters: {},
    observables: {},
    init: $.noop,
    _init : function(attrs,options){

        //prepare attrs
        this.attributes = attrs || {};
        this._preSaveAttrs = Object.clone(this.attributes);
        this._preAttrs = Object.clone(this.attributes );

        if(typeof this.init == 'function')
            this.init(attrs,options);

        var modelKey = attrs.id || attrs.model_name || $$.uid();

        ModelStorage[modelKey] = this;

        this.trigger(Model.Events.NEW,this);
    },
    has : function(key){
        return !!this.attributes[key];
    },
    set : function(key,value,opt){
        var validSetOperation = true;

        if(typeof key != 'string'){
            opt = value;
            validSetOperation =  Object.every(key,this._set.bind(this,opt));
        }
        else
            validSetOperation =  this._set(opt,key,value);

        if(!opt.silent)
            this.trigger(Model.Events.CHANGED,this.getChangedAttrs(),this);

        return validSetOperation;
    },
    getChangedAttrs : function(){
        this.filterAttrState('changed');
    },
    filterAttrState : function(filter){
        var ret = {};
        for(var key in this._attrStates){
            if(this._attrStates[key] == 'filter')
                ret[key] = this.attributes[key];
        }
        return ret;
    },
    _set : function(opt,key,value){
        if(!this.isValid(key,value)){
            this.trigger(Model.Events.NOTVALID,key,value,this);
            return false;
        }

        if(this.attributes.hasOwnProperty(key)){
            var preVal = this.attributes[key];

            if(preVal !== value && !opt.silent)
                this._attrStates[key] = 'changed';
            else
                delete this._attrStates[key];

            this._preAttrs[key] = preVal;
        }

        this.attributes[key] = value;
    },
    get : function(key){


        return this.getters[name]?this.getters[name]():this.attributes[name];
    },
    unset : function(key,opt){
        if(typeof key != 'string')
            key.each(this._unset.bind(this,opt));
        else
            this._unset(opt,key);

        if(!opt.silent)
            this.trigger(Model.Events.UNSET,this.getUnsetAttrs(),this);

    },
    _unset : function(opt,key){
        if(this.attributes.hasOwnProperty(key)){
            delete this.attributes[key];
            this._attrStates[key] = 'unset'
        }
    },
    getUnsetAttrs : function(){
        this.filterAttrState('unset');
    },
    toJSON : function(){
        return Object.clone(this.attributes);
    },
    clear : function(){},
    fetch : function(){},
    save : function(){},
    destroy : function(){},
    url : function(){},
    clone : function(){},
    previous : function(){

    },
    isValid : function(){},
    _validate : function(){},
    toJsonString : function(){},
    restoreToPrevious : function(){},
    restoreToPreSave : function(){}

});
Model.States = {
    CHANGED:1,
    PENDING:2,
    SILENT:3
}

Model.Events = {
    NEW : 'new',
    SAVING :  'saving',
    RESET : 'reset',
    DESTROYED : 'destroyed',
    CHANGED : 'changed',
    NOTVALID : 'not-valid',
    VALID : 'valid',
    UNSET : 'unset'
};

$$.View = $$.Class.extend(
	function(options) {

		this._el = $(document.createElement(this._tagName));
		if (this._className)
			this._el.addClass(this._className);
		if (this._templateId)
			this._el.html($('script#'+this._templateId+'[type="application/template"]').html());

	}, {
		// Methods
		_tagName : 'div',
		_className : null,
		_templateId : null,
		renderTo : function(el)
		{
			this._el.appendTo(el);
            this._render();
			return this;
		},
		_render : function()
		{

		},
		// Convenient Methods
		el : function() {
			return this._el;
		},
		$ : function() {
			return this._el.find.apply(this._el, arguments);
		},
        hide : function() {
            this._el.hide();
            return this;
        },
        show : function() {
            this._el.show();
            return this;
        }
	});


// Backbone.History
// ----------------

// Handles cross-browser history management, based on URL fragments. If the
// browser does not support `onhashchange`, falls back to polling.
// Cached regex for cleaning leading hashes and slashes .

var routeStripper = /^[#\/]/;

// Cached regex for detecting MSIE.
var isExplorer = /msie [\w.]+/;

var Backbone = {};

var History = Backbone.History = $$.Event.extend(
    function(){
        this.handlers = [];
        _.bindAll(this, 'checkUrl');
    },
    {

        // The default interval to poll for hash changes, if necessary, is
        // twenty times a second.
        interval: 50,

        // Gets the true hash value. Cannot use location.hash directly due to bug
        // in Firefox where location.hash will always be decoded.
        getHash: function(windowOverride) {
            var loc = windowOverride ? windowOverride.location : window.location;
            var match = loc.href.match(/#(.*)$/);
            return match ? match[1] : '';
        },

        // Get the cross-browser normalized URL fragment, either from the URL,
        // the hash, or the override.
        getFragment: function(fragment, forcePushState) {
            if (fragment == null) {
                if (this._hasPushState || forcePushState) {
                    fragment = window.location.pathname;
                    var search = window.location.search;
                    if (search) fragment += search;
                } else {
                    fragment = this.getHash();
                }
            }
            if (!fragment.indexOf(this.options.root)) fragment = fragment.substr(this.options.root.length);
            return fragment.replace(routeStripper, '');
        },

        // Start the hash change handling, returning `true` if the current URL matches
        // an existing route, and `false` otherwise.
        start: function(options) {
            if (History.started) throw new Error("Backbone.history has already been started");
            History.started = true;

            // Figure out the initial configuration. Do we need an iframe?
            // Is pushState desired ... is it available?
            this.options          = _.extend({}, {root: '/'}, this.options, options);
            this._wantsHashChange = this.options.hashChange !== false;
            this._wantsPushState  = !!this.options.pushState;
            this._hasPushState    = !!(this.options.pushState && window.history && window.history.pushState);
            var fragment          = this.getFragment();
            var docMode           = document.documentMode;
            var oldIE             = (isExplorer.exec(navigator.userAgent.toLowerCase()) && (!docMode || docMode <= 7));

            if (oldIE) {
                this.iframe = $('<iframe src="javascript:0" tabindex="-1" />').hide().appendTo('body')[0].contentWindow;
                this.navigate(fragment);
            }

            // Depending on whether we're using pushState or hashes, and whether
            // 'onhashchange' is supported, determine how we check the URL state.
            if (this._hasPushState) {
                $(window).bind('popstate', this.checkUrl);
            } else if (this._wantsHashChange && ('onhashchange' in window) && !oldIE) {
                $(window).bind('hashchange', this.checkUrl);
            } else if (this._wantsHashChange) {
                this._checkUrlInterval = setInterval(this.checkUrl, this.interval);
            }

            // Determine if we need to change the base url, for a pushState link
            // opened by a non-pushState browser.
            this.fragment = fragment;
            var loc = window.location;
            var atRoot  = loc.pathname == this.options.root;

            // If we've started off with a route from a `pushState`-enabled browser,
            // but we're currently in a browser that doesn't support it...
            if (this._wantsHashChange && this._wantsPushState && !this._hasPushState && !atRoot) {
                this.fragment = this.getFragment(null, true);
                window.location.replace(this.options.root + '#' + this.fragment);
                // Return immediately as browser will do redirect to new url
                return true;

                // Or if we've started out with a hash-based route, but we're currently
                // in a browser where it could be `pushState`-based instead...
            } else if (this._wantsPushState && this._hasPushState && atRoot && loc.hash) {
                this.fragment = this.getHash().replace(routeStripper, '');
                window.history.replaceState({}, document.title, loc.protocol + '//' + loc.host + this.options.root + this.fragment);
            }

            if (!this.options.silent) {
                return this.loadUrl();
            }
        },

        // Disable Backbone.history, perhaps temporarily. Not useful in a real app,
        // but possibly useful for unit testing Routers.
        stop: function() {
            $(window).unbind('popstate', this.checkUrl).unbind('hashchange', this.checkUrl);
            clearInterval(this._checkUrlInterval);
            History.started = false;
        },

        // Add a route to be tested when the fragment changes. Routes added later
        // may override previous routes.
        route: function(route, callback) {
            this.handlers.unshift({route: route, callback: callback});
        },

        // Checks the current URL to see if it has changed, and if it has,
        // calls `loadUrl`, normalizing across the hidden iframe.
        checkUrl: function(e) {
            var current = this.getFragment();
            if (current == this.fragment && this.iframe) current = this.getFragment(this.getHash(this.iframe));
            if (current == this.fragment) return false;
            if (this.iframe) this.navigate(current);
            this.loadUrl() || this.loadUrl(this.getHash());
        },

        // Attempt to load the current URL fragment. If a route succeeds with a
        // match, returns `true`. If no defined routes matches the fragment,
        // returns `false`.
        loadUrl: function(fragmentOverride) {
            var fragment = this.fragment = this.getFragment(fragmentOverride);
            var matched = _.any(this.handlers, function(handler) {
                if (handler.route.test(fragment)) {
                    handler.callback(fragment);
                    return true;
                }
            });
            return matched;
        },

        // Save a fragment into the hash history, or replace the URL state if the
        // 'replace' option is passed. You are responsible for properly URL-encoding
        // the fragment in advance.
        //
        // The options object can contain `trigger: true` if you wish to have the
        // route callback be fired (not usually desirable), or `replace: true`, if
        // you wish to modify the current URL without adding an entry to the history.
        navigate: function(fragment, options) {
            if (!History.started) return false;
            if (!options || options === true) options = {trigger: options};
            var frag = (fragment || '').replace(routeStripper, '');
            if (this.fragment == frag) return;

            // If pushState is available, we use it to set the fragment as a real URL.
            if (this._hasPushState) {
                if (frag.indexOf(this.options.root) != 0) frag = this.options.root + frag;
                this.fragment = frag;
                window.history[options.replace ? 'replaceState' : 'pushState']({}, document.title, frag);

                // If hash changes haven't been explicitly disabled, update the hash
                // fragment to store history.
            } else if (this._wantsHashChange) {
                this.fragment = frag;
                this._updateHash(window.location, frag, options.replace);
                if (this.iframe && (frag != this.getFragment(this.getHash(this.iframe)))) {
                    // Opening and closing the iframe tricks IE7 and earlier to push a history entry on hash-tag change.
                    // When replace is true, we don't want this.
                    if(!options.replace) this.iframe.document.open().close();
                    this._updateHash(this.iframe.location, frag, options.replace);
                }

                // If you've told us that you explicitly don't want fallback hashchange-
                // based history, then `navigate` becomes a page refresh.
            } else {
                window.location.assign(this.options.root + fragment);
            }
            if (options.trigger) this.loadUrl(fragment);
        },

        // Update the hash location, either replacing the current entry, or adding
        // a new one to the browser history.
        _updateHash: function(location, fragment, replace) {
            if (replace) {
                location.replace(location.toString().replace(/(javascript:|#).*$/, '') + '#' + fragment);
            } else {
                location.hash = fragment;
            }
        }
    }
);

// Has the history handling already been started?
History.started = false;

// Cached regular expressions for matching named param parts and splatted
// parts of route strings.
var namedParam    = /:\w+/g;
var splatParam    = /\*\w+/g;
var escapeRegExp  = /[-[\]{}()+?.,\\^$|#\s]/g;

//sample state url

//#page-hash$$tab[index=2]/table[page=3]&form[admin=false]


var statePropertyAbbreviate = {
    Pattern : 'p',
    Children : 'c',
    Vars  : 'v'
};

$$.State = $$.Event.extend(function(options){
    this.pattern = options.pattern;;
    this.name = options.name;
    this.callback = options.callback;
    this._childStates = {};
    this._numChildStates = 0;

    if(options.states)
        this.setSubStates(options.states);

},{
    setSubStates : function(states){
        states.each(this.setSubState.bind(this));
    },
    setSubState : function(state){
        if(!(state instanceof $$.State))
            state = new $$.State(state);

        this._childStates[state.name] = state;
        this._numChildStates ++;
    },
    getSubState : function(stateName){
        if(this._childStates[stateName])
            return this._childStates[stateName];
        else
            return false;
    },
    matchState : function(state){
        if(this._isMatchedState(state)){
            this._params = this._extractStateParams(state);

            this.callback && this.callback.call(this,this._params);

            var subStates = state[statePropertyAbbreviate.Children];

            if(subStates){
                for(var i = 0; i < subStates.length; i++){
                    var subState = subStates[i];
                    for(var childStateName in this._childStates ){
                        if(this._childStates[childStateName].matchState(subState))
                            break;
                    }
                }
            };

            this.trigger($$.State.Events.STATE_TRIGGERED,this,state);

            return true;
        }
        else
            return false;
    },
    _isMatchedState : function(state){
        if(state.hasOwnProperty(statePropertyAbbreviate.Pattern)){
            //$$.log(state[statePropertyAbbreviate.Pattern] , this.pattern)
            return state[statePropertyAbbreviate.Pattern] == this.pattern;
        }
        else
            return false;
    },
    _extractStateParams : function(state){
        return state[statePropertyAbbreviate.Vars] || {};
    },
    setParams : function(params,replaceAll){
        replaceAll = !!replaceAll;

        this._params = replaceAll ? params : $.extend(this._params,params);

        $$.Events.trigger($$.State.Events.STATE_CHANGED);
    },
    getParams : function(){
        return this._params || {};
    },
    toObject : function(){
        var ret = {};

        ret[statePropertyAbbreviate.Pattern] = this.pattern;
        ret[statePropertyAbbreviate.Vars] = this._params;

        if(this._numChildStates){
            var childState = [];
            for(var childStateName in this._childStates ){
                childState.push(this._childStates[childStateName].toObject())
            }
            ret[statePropertyAbbreviate.Children] = childState;
        }

        return ret;
    }
});

$$.State.Events = {
    STATE_TRIGGERED : 'owl.state.events.state_triggered',
    STATE_CHANGED : 'owl.state.events.state_changed'
};

$$.Router = $$.Event.extend(
    function(options){
        options || (options = {});
        this._routeStates = {};
        if (options.routes) this.routes = options.routes;
        this._bindRoutes();
        this.initialize.apply(this, arguments);
    },{
        initialize: function(){},
        route: function(route, name, callback, states) {
            $$.history || ($$.history = new Backbone.History());

            if (!_.isRegExp(route)) route = this._routeToRegExp(route);

            if (!callback) callback = this[name];

            this._hasStates = !!states;

            if ( this._hasStates )
                this._prepareStates(name,states);

            $$.history.route(route, _.bind(function(fragment) {
                //$$.log('in')

                this._route = this._parseHash(fragment);

                var args = this._extractParameters(route, fragment);
                callback && callback.apply(this, args);

                if(this._route['sub'])
                    this._iterateStates(name,this._route['sub']);

                this.trigger.apply(this, ['route:' + name].concat(args));
                $$.history.trigger('route', this, name, args);
            }, this));

            return this;
        },
        _iterateStates : function(routeName,fragment){

            if(fragment){
                try{
                    var states = JSON.parse(decodeURI(fragment));

                    for(var i = 0; i < states.length; i++){
                        var state = states[i];

                        var routeStates = this._routeStates[routeName];

                        for(var stateName in routeStates){
                            if(routeStates[stateName].matchState(state))
                                break;
                        }
                    }
                }
                catch(e){
                    throw e;
                }

            }
        },
        _parseHash : function(fragment){
            var pageStateRegExp = /(\w+)(\$\$(\S*))?$/g,
                regExec = pageStateRegExp.exec(fragment);

            var ret = {};

            if(regExec[1])
                ret.main = regExec[1];
            if(regExec[2] && regExec[3])
                ret.sub = regExec[3];

            return ret;

        },
        _extractStates : function(fragment){

            var result;
            if(result = validStatesFormat.exec(fragment))
                return result[1];
            else
                return false;
        },
        _prepareStates : function(routeName, states){
            this._routeStates[routeName] || (this._routeStates[routeName] = {});

            var $this = this;

            states.each(function(state){

                if(!(state instanceof $$.State))
                    state = new $$.State(state);

                $this._routeStates[routeName][state.name] = state;

                $$.Events.bind($$.State.Events.STATE_CHANGED, $this._onStateParamChange.bind($this));

            });
        },
        _onStateParamChange : function(){
            if(this._route)
                this.navigate(this._route.main,{keepStates : true});
            //$$.log('new states', states)
        },
        _getStatesParams : function(){
            var states = [];

            for(var routeName in this._routeStates){
                var routeStates = this._routeStates[routeName];

                for(var stateName in routeStates){
                    states.push(routeStates[stateName].toObject());
                }
            }

            return states;
        },
        _attachStatesParams : function(fragment){
            return fragment + '$$' + JSON.stringify(this._getStatesParams())
        },
        getState : function(routeName){
            return this._routeStates[routeName] || false;
        },
        // Simple proxy to `Backbone.history` to save a fragment into the history.
        navigate: function(fragment, options) {
            if(options.keepStates)
                fragment = this._attachStatesParams(fragment);

            $$.history.navigate(fragment, options);
        },

        // Bind all defined routes to `Backbone.history`. We have to reverse the
        // order of the routes here to support behavior where the most general
        // routes can be defined at the bottom of the route map.
        _bindRoutes: function() {
            if (!this.routes) return;
            var routes = [];
            for (var route in this.routes) {
                routes.unshift([route, this.routes[route]]);
            }
            for (var i = 0, l = routes.length; i < l; i++) {
                this.route(routes[i][0], routes[i][1], this[routes[i][1]]);
            }
        },

        // Convert a route string into a regular expression, suitable for matching
        // against the current location hash.
        _routeToRegExp: function(route) {
            route = route.replace(escapeRegExp, '\\$&')
                .replace(namedParam, '([^\/]+)')
                .replace(splatParam, '(.*?)');

            return new RegExp('^' + route + '(\\$\\$(\\S*))?');
        },

        // Given a route, and a URL fragment that it matches, return the array of
        // extracted parameters.
        _extractParameters: function(route, fragment) {
            return route.exec(fragment).slice(1);
        }

    }
);

}(Owl))