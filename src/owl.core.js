/*!
 * Owl.js JavaScript Library
 *
 * Copyright 2012, CodeParticle Inc
 * All Rights Reserved.
 */
(function($) {

    var oldContext = this;
    var old$$ = this.$$;
    var Owl = {

        noConflict : function() {
            oldContext.$$ = old$$;
        },

        log : function () {
            this.debug.apply(this, arguments);
        },

        debug: function() {
            this.Logger._log(this.Logger.level.DEBUG, arguments);
        },
        info: function() {
            this.Logger._log(this.Logger.level.INFO, arguments);
        },
        warn: function() {
            this.Logger._log(this.Logger.level.WARN, arguments);
        },
        error: function() {
            this.Logger._log(this.Logger.level.ERROR, arguments);
        },
        fatal: function() {
            this.Logger._log(this.Logger.level.FATAL, arguments);
        },

        write : function() {
            for(var i = 0, j = arguments.length; i < j; i++)
                document.write(arguments[i]+'<br />');
        },
        _idCount: 0,
        uid: function(prefix) {
            if (!prefix) prefix = "";
            return prefix+(++this._idCount);
        },

        logLevel:0

    };

    Owl.Logger = {
        level: {
            DEBUG: 0,
            INFO: 1,
            WARN: 2,
            ERROR: 3,
            FATAL:4
        },
        _log: function(lvl, args) {
            if (typeof console == 'undefined' || lvl < Owl.logLevel) return false;
            try {
                if (args.length > 1) {
                    console.dir(args);
                } else console.log(args[0]);
            }
            catch (e) {}
        }
    }

    this.Owl = this.$$ = Owl;

}) (jQuery);

/*!
 * Owl.js JavaScript Library
 *
 * Copyright 2012, CodeParticle Inc
 * All Rights Reserved.
 */
(function(){

    var root = this;

// Establish the object that gets returned to break out of a loop iteration.
    var breaker = {};

// Save bytes in the minified (but not gzipped) version:
    var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;


// All **ECMAScript 5** native function implementations that we hope to use
// are declared here.
    var
        nativeForEach      = ArrayProto.forEach,
        nativeMap          = ArrayProto.map,
        nativeReduce       = ArrayProto.reduce,
        nativeReduceRight  = ArrayProto.reduceRight,
        nativeFilter       = ArrayProto.filter,
        nativeEvery        = ArrayProto.every,
        nativeSome         = ArrayProto.some,
        nativeIndexOf      = ArrayProto.indexOf,
        nativeLastIndexOf  = ArrayProto.lastIndexOf,
        nativeIsArray      = Array.isArray,
        nativeKeys         = Object.keys,
        nativeBind         = FuncProto.bind;

// Create quick reference variables for speed access to core prototypes.
    var
        slice            = ArrayProto.slice,
        unshift          = ArrayProto.unshift,
        toString         = ObjProto.toString,
        hasOwnProperty   = ObjProto.hasOwnProperty;



////////////////////////////////////////
//
//	Collection


// The cornerstone, an `each` implementation, aka `forEach`.
// Handles objects with the built-in `forEach`, arrays, and raw objects.
// Delegates to **ECMAScript 5**'s native `forEach` if available.
    var each = function(iterator,context) {
        var obj = this;
        if (obj == null) return;
        if (false && nativeForEach && obj.forEach === nativeForEach) {
            obj.forEach(iterator, context);
        } else if (obj.length === +obj.length) {
            for (var i = 0, l = obj.length; i < l; i++) {
                if (i in obj && iterator.call(context, obj[i], i, obj) === breaker) return;
            }
        } else {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    if (iterator.call(context, obj[key], key, obj) === breaker) return;
                }
            }
        }
    };

// Return the results of applying the iterator to each element.
// Delegates to **ECMAScript 5**'s native `map` if available.
    var map = function(iterator,context) {
        var obj = this;
        var results = [];
        if (obj == null) return results;
        if (nativeMap && obj.map === nativeMap) return obj.map(iterator, context);
        each.call(obj, function(value, index, list) {
            results[results.length] = iterator.call(context, value, index, list);
        });
        if (obj.length === +obj.length) results.length = obj.length;
        return results;
    };

// Determine if at least one element in the object matches a truth test.
// Delegates to **ECMAScript 5**'s native `some` if available.
// Aliased as `any`.
    var any = function(iterator, context) {
        var obj = this;
        iterator || (iterator = identity);
        var result = false;
        if (obj == null) return result;
        if (nativeSome && obj.some === nativeSome) return obj.some(iterator, context);
        each.call(obj, function(value, index, list) {
            if (result || (result = iterator.call(context, value, index, list))) return breaker;
        });
        return !!result;
    };

// Determine whether all of the elements match a truth test.
// Delegates to **ECMAScript 5**'s native `every` if available.
// Aliased as `all`.
    var every = function(iterator, context) {
        var obj = this;
        var result = true;
        if (obj == null) return result;
        if (nativeEvery && obj.every === nativeEvery) return obj.every(iterator, context);
        each.call(obj, function(value, index, list) {
            if (!(result = result && iterator.call(context, value, index, list))) return breaker;
        });
        return result;
    };

// Return the first value which passes a truth test. Aliased as `detect`.
    var find  = function(iterator, context) {
        var obj = this;
        var result;
        any.call(obj, function(value, index, list) {
            if (iterator.call(context, value, index, list)) {
                result = value;
                return true;
            }
        });
        return result;
    };

// Return all the elements that pass a truth test.
// Delegates to **ECMAScript 5**'s native `filter` if available.
// Aliased as `select`.
    var filter =  function(iterator, context) {
        var obj = this;
        var results = [];
        if (obj == null) return results;
        if (nativeFilter && obj.filter === nativeFilter) return obj.filter(iterator, context);
        each.call(obj, function(value, index, list) {
            if (iterator.call(context, value, index, list)) results[results.length] = value;
        });
        return results;
    };

// Return all the elements for which a truth test fails.
    var reject = function(iterator, context) {
        var obj = this;
        var results = [];
        if (obj == null) return results;
        each.call(obj, function(value, index, list) {
            if (!iterator.call(context, value, index, list)) results[results.length] = value;
        });
        return results;
    };

// Determine if a given value is included in the array or object using `===`.
// Aliased as `contains`.
    var include = function(target) {
        var obj = this;
        var found = false;
        if (obj == null) return found;
        if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1;
        found = any.call(obj, function(value) {
            return value === target;
        });
        return found;
    };

// Convenience version of a common use case of `map`: fetching a property.
    var pluck = function(key) {
        var obj = this;
        return map.call(obj, function(value){return value[key]; });
    };

// Invoke a method (with arguments) on every item in a collection.
    var invoke = function(method) {
        var obj = this;
        var args = slice.call(arguments, 1);
        return map.call(obj, function(value) {
          return (isFunction(method) ? method || value : value[method]).apply(value, args);
        });
    };

////////////////////////////////////////
//
//	Arrays

// Return the maximum element or (element-based computation).
    var max = function(iterator, context) {
        var obj = this;
        if (!iterator && typeof obj == 'array') return Math.max.apply(Math, obj);
        if (!iterator && isEmpty(obj)) return -Infinity;
        var result = {computed : -Infinity};
        each.call(obj, function(value, index, list) {
            var computed = iterator ? iterator.call(context, value, index, list) : value;
            computed >= result.computed && (result = {value : value, computed : computed});
        });
        return result.value;
    };

// Return the minimum element (or element-based computation).
    var min = function(iterator, context) {
        var obj = this;
        if (!iterator && typeof obj == 'array') return Math.min.apply(Math, obj);
        if (!iterator && isEmpty(obj)) return Infinity;
        var result = {computed : Infinity};
        each.call(obj, function(value, index, list) {
            var computed = iterator ? iterator.call(context, value, index, list) : value;
            computed < result.computed && (result = {value : value, computed : computed});
        });
        return result.value;
    };

// Shuffle an array.
    var shuffle = function() {
        var obj = this;

        var shuffled = [], rand;
        each.call(obj, function(value, index, list) {
            if (index == 0) {
                shuffled[0] = value;
            } else {
                rand = Math.floor(Math.random() * (index + 1));
                shuffled[index] = shuffled[rand];
                shuffled[rand] = value;
            }
        });
        return shuffled;
    };

// Sort the object's values by a criterion produced by an iterator.
    var sortBy = function(iterator, context) {
        var obj = this;
        return pluck.call(map.call(obj, function(value, index, list) {
            return {
                value : value,
                criteria : iterator.call(context, value, index, list)
            };
        }).sort(function(left, right) {
                var a = left.criteria, b = right.criteria;
                return a < b ? -1 : a > b ? 1 : 0;
            }), 'value');
    };

// Groups the object's values by a criterion. Pass either a string attribute
// to group by, or a function that returns the criterion.
    var groupBy = function(val) {
        var obj = this;
        var result = {};
        var iterator = typeof val == 'function' ? val : function(obj) { return obj[val]; };
        each.call(obj, function(value, index) {
            var key = iterator(value, index);
            (result[key] || (result[key] = [])).push(value);
        });
        return result;
    };
// **Reduce** builds up a single result from a list of values, aka `inject`,
// or `foldl`. Delegates to **ECMAScript 5**'s native `reduce` if available.
    var reduce = function(iterator,memo,context) {
        var obj = this;
        var initial = arguments.length > 1;
        if (obj == null) obj = [];
        if (nativeReduce && obj.reduce === nativeReduce) {
            if (context) iterator = iterator.bind(context);
            return initial ? obj.reduce(iterator, memo) : obj.reduce(iterator);
        }
        each.call(obj, function(value, index, list) {
            if (!initial) {
                memo = value;
                initial = true;
            } else {
                memo = iterator.call(context, memo, value, index, list);
            }
        });
        if (!initial) throw new TypeError('Reduce of empty array with no initial value');
        return memo;
    };

// Use a comparator function to figure out at what index an object should
// be inserted so as to maintain order. Uses binary search.
    var sortedIndex = function(obj, iterator) {
        var array = this;
        iterator || (iterator = identity);
        var low = 0, high = array.length;
        while (low < high) {
            var mid = (low + high) >> 1;
            iterator(array[mid]) < iterator(obj) ? low = mid + 1 : high = mid;
        }
        return low;
    };
// Get the first element of an array. Passing **n** will return the first N
// values in the array. Aliased as `head`. The **guard** check allows it to work
// with `_.map`.
    var first = function(n, guard) {
        var array = this;
        return (n != null) && !guard ? slice.call(array, 0, n) : array[0];
    };

// Returns everything but the last entry of the array. Especcialy useful on
// the arguments object. Passing **n** will return all the values in
// the array, excluding the last N. The **guard** check allows it to work with
// `_.map`.
    var initial = function(n, guard) {
        var array = this;
        return slice.call(array, 0, array.length - ((n == null) || guard ? 1 : n));
    };

// Get the last element of an array. Passing **n** will return the last N
// values in the array. The **guard** check allows it to work with `_.map`.
    var last = function(n, guard) {
        var array = this;
        if ((n != null) && !guard) {
            return slice.call(array, Math.max(array.length - n, 0));
        } else {
            return array[array.length - 1];
        }
    };

// Returns everything but the first entry of the array. Aliased as `tail`.
// Especially useful on the arguments object. Passing an **index** will return
// the rest of the values in the array from that index onward. The **guard**
// check allows it to work with `_.map`.
    var rest = function(index, guard) {
        var array = this;
        return slice.call(array,(index == null) || guard ? 1 : index);
    };

// Trim out all falsy values from an array.
    var compact = function() {
        var array = this;
        return filter.call(array,function(value){ return !!value; });
    };

// Return a completely flattened version of an array.
    var flatten = function(shallow) {
        var array = this;
        return reduce.call(array, function(memo, value) {
            if (typeof value == 'array') return memo.concat(shallow ? value : flatten.call(value,shallow));
            memo[memo.length] = value;
            return memo;
        }, []);
    };

// Return a version of the array that does not contain the specified value(s).
    var without = function() {
        var array = this;
        return difference.call(array, slice.call(arguments, 0));
    };

// Produce a duplicate-free version of the array. If the array has already
// been sorted, you have the option of using a faster algorithm.
// Aliased as `unique`.
    var uniq =  function(isSorted, iterator) {
        var array = this;
        var initial = iterator ? map.call(array, iterator) : array;
        var result = [];
        reduce.call(initial, function(memo, el, i) {
            if (0 == i || (isSorted === true ? _.last(memo) != el : !include.call(memo, el))) {
                memo[memo.length] = el;
                result[result.length] = array[i];
            }
            return memo;
        }, []);
        return result;
    };

// Produce an array that contains the union: each distinct element from all of
// the passed-in arrays.
    var union = function() {
        return uniq.call(flatten.call(arguments, true));
    };

// Produce an array that contains every item shared between all the
// passed-in arrays. (Aliased as "intersect" for back-compat.)
    var intersection =  function() {
        var array = this;
        var rest = arguments;
        return filter.call(uniq.call(array), function(item) {
            return every.call(rest, function(other) {
                return indexOf.call(other, item) >= 0;
            });
        });
    };

// Take the difference between one array and a number of other arrays.
// Only the elements present in just the first array will remain.
    var difference = function() {
        var array = this;
        var rest = flatten.call(arguments);
        return filter.call(array, function(value){ return !include.call(rest, value); });
    };

// Zip together multiple lists into a single array -- elements that share
// an index go together.
    var zip = function() {
        var args = arguments;
        var length = max.call(pluck.call(args, 'length'));
        var results = new Array(length);
        for (var i = 0; i < length; i++) results[i] = pluck.call(args, "" + i);
        return results;
    };

// If the browser doesn't supply us with indexOf (I'm looking at you, **MSIE**),
// we need this function. Return the position of the first occurrence of an
// item in an array, or -1 if the item is not included in the array.
// Delegates to **ECMAScript 5**'s native `indexOf` if available.
// If the array is large and already in sort order, pass `true`
// for **isSorted** to use binary search.
    var indexOf = function(item, isSorted) {
        var array = this;
        if (array == null) return -1;
        var i, l;
        if (isSorted) {
            i = sortedIndex.call(array, item);
            return array[i] === item ? i : -1;
        }
        if (nativeIndexOf && array.indexOf === nativeIndexOf) return array.indexOf(item);
        for (i = 0, l = array.length; i < l; i++) if (i in array && array[i] === item) return i;
        return -1;
    };
// Delegates to **ECMAScript 5**'s native `lastIndexOf` if available.
    var lastIndexOf = function(item) {
        var array = this;
        if (array == null) return -1;
        if (nativeLastIndexOf && array.lastIndexOf === nativeLastIndexOf) return array.lastIndexOf(item);
        var i = array.length;
        while (i--) if (i in array && array[i] === item) return i;
        return -1;
    };
// Generate an integer Array containing an arithmetic progression. A port of
// the native Python `range()` function. See
// [the Python documentation](http://docs.python.org/library/functions.html#range).
    var range = function(start, stop, step) {
        if (arguments.length <= 1) {
            stop = start || 0;
            start = 0;
        }
        step = arguments[2] || 1;

        var len = Math.max(Math.ceil((stop - start) / step), 0);
        var idx = 0;
        var range = new Array(len);

        while(idx < len) {
            range[idx++] = start;
            start += step;
        }

        return range;
    };
////////////////////////////////////////
//
//	Function

    /**
     * .bind(context [, arg, arg, arg, ...])
     * @param	context (object) - The context the function will be invoked with.
     * @param	arg (mixed) - Any number of arguments that will be bound to the function invocation. Any arguments subsequently passed to the bound function will be appended after the arg's.
     */
    var bind = (function(){

        var _slice = Array.prototype.slice;

        return function(context) {
            var fn = this,
                args = _slice.call(arguments, 1);

            if (args.length) {
                return function() {
                    return arguments.length
                        ? fn.apply(context, args.concat(_slice.call(arguments)))
                        : fn.apply(context, args);
                };
            }
            return function() {
                return arguments.length
                    ? fn.apply(context, arguments)
                    : fn.call(context);
            };
        };
    })();
// Memoize an expensive function by storing its results.
    var memoize = function(hasher) {
        var func = this;
        var memo = {};
        hasher || (hasher = identity);
        return function() {
            var key = hasher.apply(this, arguments);
            return memo.hasOwnProperty(key) ? memo[key] : (memo[key] = func.apply(this, arguments));
        };
    };
// Delays a function for the given number of milliseconds, and then calls
// it with the arguments supplied.
    var delay = function(wait) {
        var func = this;
        var args = slice.call(arguments, 1);
        return setTimeout(function(){ return func.apply(func, args); }, wait);
    };
// Defers a function, scheduling it to run after the current call stack has
// cleared.
    var defer = function() {
        var func = this;
        return delay.apply(func, [1].concat(arguments));
    };

// Returns a function, that, when invoked, will only be triggered at most once
// during a given window of time.
    var throttle = function(wait) {
        var func = this;
        var context, args, timeout, throttling, more;
        var whenDone = debounce.call(function(){ more = throttling = false; }, wait);
        return function() {
            context = this; args = arguments;
            var later = function() {
                timeout = null;
                if (more) func.apply(context, args);
                whenDone();
            };
            if (!timeout) timeout = setTimeout(later, wait);
            if (throttling) {
                more = true;
            } else {
                func.apply(context, args);
            }
            whenDone();
            throttling = true;
        };
    };
// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds.
    var debounce = function(wait) {
        var func = this;
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                func.apply(context, args);
            };
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
        };
    };
// Returns a function that will be executed at most one time, no matter how
// often you call it. Useful for lazy initialization.
    var once = function() {
        var func = this;
        var ran = false, memo;
        return function() {
            if (ran) return memo;
            ran = true;
            return memo = func.apply(this, arguments);
        };
    };
// Returns a function that will only be executed after being called N times.
    var after = function(times) {
        var func = this;
        if (times <= 0) return func();
        return function() {
            if (--times < 1) { return func.apply(this, arguments); }
        };
    };

//////////////////////////////////////////////
//
//	Object

// Retrieve the names of an object's properties.
// Delegates to **ECMAScript 5**'s native `Object.keys`
    var keys = nativeKeys || function(obj) {
        if (obj !== Object(obj)) throw new TypeError('Invalid object');
        var keys = [];
        for (var key in obj) if (has(obj, key)) keys[keys.length] = key;
        return keys;
    };

// Retrieve the values of an object's properties.
    var values = function(obj) {
        return map.call(obj, identity);
    };

    var typeOf = function(obj){
        return typeof obj;
    };

// Return a sorted list of the function names available on the object.
// Aliased as `methods`
    var functions = function(obj) {
        var names = [];
        for (var key in obj) {
            if (isFunction(obj[key])) names.push(key);
        }
        return names.sort();
    };

// Has own property?
    var has = function(obj, key) {
        return hasOwnProperty.call(obj, key);
    };

// Is a given value a function?
    var isFunction = function(obj) {
        return typeOf(obj) == 'function';
    };

// Is a given array, string, or object empty?
// An "empty" object has no enumerable own-properties.
    var isEmpty = function(obj) {
        if (_.isArray(obj) || _.isString(obj)) return obj.length === 0;
        for (var key in obj) if (_.has(obj, key)) return false;
        return true;
    };

// Is a given value a DOM element?
    var isElement = function(obj) {
        return typeOf(obj) == 'element';
    };

// Is a given value an array?
// Delegates to ECMA5's native Array.isArray
    var isArray = nativeIsArray || function(obj) {
        return typeOf(obj) == 'array';
    };

// Is a given variable an object?
    var isObject = function(obj) {
        return typeOf(obj) == 'object';
    };

// Is a given variable an arguments object?
    var isArguments = function(obj) {
        return typeOf(obj) == 'arguments';
    };

// Is a given value a string?
    var isString = function(obj) {
        return typeOf(obj) == 'string';
    };

// Is a given value a number?
    var isNumber = function(obj) {
        return typeOf(obj) == 'number';
    };

// Is the given value `NaN`?
    var isNaN = function(obj) {
        // `NaN` is the only value for which `===` is not reflexive.
        return obj !== obj;
    };

// Is a given value a boolean?
    var isBoolean = function(obj) {
        return obj === true || obj === false || typeOf(obj) == 'boolean';
    };

// Is a given value a date?
    var isDate = function(obj) {
        return typeOf(obj) == 'arguments';
    };

// Is the given value a regular expression?
    var isRegExp = function(obj) {
        return toString.call(obj) == '[object RegExp]';
    };

// Is a given value equal to null?
    var isNull = function(obj) {
        return obj === null;
    };

// Is a given variable undefined?
    var isUndefined = function(obj) {
        return obj === void 0;
    };

// Is a given variable undefined or null
    var isNullOrUndefined = function(obj) {
        return isUndefined(obj) || isNull(obj);
    };
// Internal recursive comparison function.
    function eq(a, b, stack) {
        // Identical objects are equal. `0 === -0`, but they aren't identical.
        // See the Harmony `egal` proposal: http://wiki.ecmascript.org/doku.php?id=harmony:egal.
        if (a === b) return a !== 0 || 1 / a == 1 / b;
        // A strict comparison is necessary because `null == undefined`.
        if (a == null || b == null) return a === b;
        // Unwrap any wrapped objects.
        if (a._chain) a = a._wrapped;
        if (b._chain) b = b._wrapped;
        // Invoke a custom `isEqual` method if one is provided.
        if (a.isEqual && isFunction(a.isEqual)) return a.isEqual(b);
        if (b.isEqual && isFunction(b.isEqual)) return b.isEqual(a);
        // Compare `[[Class]]` names.
        var className = toString.call(a);
        if (className != toString.call(b)) return false;
        switch (className) {
            // Strings, numbers, dates, and booleans are compared by value.
            case '[object String]':
                // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
                // equivalent to `new String("5")`.
                return a == String(b);
            case '[object Number]':
                // `NaN`s are equivalent, but non-reflexive. An `egal` comparison is performed for
                // other numeric values.
                return a != +a ? b != +b : (a == 0 ? 1 / a == 1 / b : a == +b);
            case '[object Date]':
            case '[object Boolean]':
                // Coerce dates and booleans to numeric primitive values. Dates are compared by their
                // millisecond representations. Note that invalid dates with millisecond representations
                // of `NaN` are not equivalent.
                return +a == +b;
            // RegExps are compared by their source patterns and flags.
            case '[object RegExp]':
                return a.source == b.source &&
                    a.global == b.global &&
                    a.multiline == b.multiline &&
                    a.ignoreCase == b.ignoreCase;
        }
        if (typeof a != 'object' || typeof b != 'object') return false;
        // Assume equality for cyclic structures. The algorithm for detecting cyclic
        // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.
        var length = stack.length;
        while (length--) {
            // Linear search. Performance is inversely proportional to the number of
            // unique nested structures.
            if (stack[length] == a) return true;
        }
        // Add the first object to the stack of traversed objects.
        stack.push(a);
        var size = 0, result = true;
        // Recursively compare objects and arrays.
        if (className == '[object Array]') {
            // Compare array lengths to determine if a deep comparison is necessary.
            size = a.length;
            result = size == b.length;
            if (result) {
                // Deep compare the contents, ignoring non-numeric properties.
                while (size--) {
                    // Ensure commutative equality for sparse arrays.
                    if (!(result = size in a == size in b && eq(a[size], b[size], stack))) break;
                }
            }
        } else {
            // Objects with different constructors are not equivalent.
            if ('constructor' in a != 'constructor' in b || a.constructor != b.constructor) return false;
            // Deep compare objects.
            for (var key in a) {
                if (has(a, key)) {
                    // Count the expected number of properties.
                    size++;
                    // Deep compare each member.
                    if (!(result = has(b, key) && eq(a[key], b[key], stack))) break;
                }
            }
            // Ensure that both objects contain the same number of properties.
            if (result) {
                for (key in b) {
                    if (has(b, key) && !(size--)) break;
                }
                result = !size;
            }
        }
        // Remove the first object from the stack of traversed objects.
        stack.pop();
        return result;
    }

// Perform a deep comparison to check if two objects are equal.
    var isEqual = function(a, b) {
        return eq(a, b, []);
    };
////////////////////////////////////////
//
//	Utils

// Is a given array, string, or object empty?
// An "empty" object has no enumerable own-properties.
    var isEmpty = function(obj) {
        if (typeof obj == 'array' || typeof obj == 'string') return obj.length === 0;
        for (var key in obj) if (obj.hasOwnProperty(key)) return false;
        return true;
    };

// Keep the identity function around for default iterators.
    var identity = function(value) {
        return value;
    };


////////////////////////////////////////
//
//	Methods to Prototypes Attachments

// Object Function Params Helper
    var ObjParamHelper = function(method){
        return function(obj){
            return method.apply(obj, slice.call(arguments, 1));
        }
    };


    var implement = function(o,hashmap){
        for (var key in hashmap){
            o[key] = hashmap[key];
        }
    };

    implement($$,{
        each : ObjParamHelper(each),
        map : ObjParamHelper(map),
        any : ObjParamHelper(any),
        find : ObjParamHelper(find),
        filter : ObjParamHelper(filter),
        reject : ObjParamHelper(reject),
        every : ObjParamHelper(every),
        include : ObjParamHelper(include),
        invoke : ObjParamHelper(invoke),
        pluck : pluck,
        keys : keys,
        values : values,
        functions: functions,
        has : has,
        isFunction : isFunction,
        isEmpty : isEmpty,
        isElement : isElement,
        isArray : isArray,
        isObject : isObject,
        isArguments : isArguments,
        isString  : isString,
        isNumber : isNumber,
        isNaN : isNaN,
        isBoolean :isBoolean,
        isDate : isDate,
        isRegExp :isRegExp,
        isNull : isNull,
        isUndefined : isUndefined,
        isNullOrUndefined : isNullOrUndefined,
        isEqual : isEqual
    });

    implement(ArrayProto,{
        each: each,
        map : map,
        reduce : reduce,
        any : any,
        find : find,
        filter : filter,
        reject : reject,
        every : every,
        include : include,
        pluck : pluck,
        max : max,
        min : min,
        shuffle : shuffle,
        sortBy : sortBy,
        groupBy : groupBy,
        sortedIndex : sortedIndex,
        first : first,
        initial : initial,
        last : last,
        rest : rest,
        compact : compact,
        flatten : flatten,
        without : without,
        uniq : uniq,
        union : union,
        intersection : intersection,
        difference : difference,
        indexOf : indexOf,
        lastIndexOf : lastIndexOf,
        invoke : invoke

    });

    implement(Array,{
        zip : zip,
        range : range
    });

    implement(FuncProto,{
        bind : bind,
        memoize : memoize,
        delay : delay,
        defer : defer,
        throttle : throttle,
        debounce : debounce,
        once : once,
        after : after
    });


}());

////////////////////////////////////////
//
//	Misc Helper Functions

(function(){

    String.format = function () {
        var s = arguments[0];
        for (var i = 0; i < arguments.length - 1; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, arguments[i + 1]);
        }
        return s;
    };

    $$.VariableStorage  = {
        createStorage : function(context,blueprint){
            $.extend(context,this._createStorage(blueprint))
        },
        _createStorage : function(blueprint){
            var storage = {};

            for(var keyName in blueprint ){
                var keyDef = blueprint[keyName];
                storage[keyName] = new $$.VariableStorage.Key(keyDef);
             }

            return storage;
        }
    };

    $$.VariableStorage.Key = function(keyDef){
        this._storage = {};
        this._keyDef = keyDef
    };

    $.extend($$.VariableStorage.Key.prototype,{
        getValue : function(key){
            return this._storage[key];
        },
        getValues : function(){
            return this._storage;
        },
        setValue : function(key,data){
            this._storage[key] = data;

            if(this._keyDef){
                for(var keyName in this._keyDef){
                    data[keyName] = $$.VariableStorage.createStorage(this,this._keyDef[keyName]);
                }
            }

        },
        setValues : function(values){
            for(var key in values)
                this.setValue(key,values[key]);
        }
    });

}());