/*!
 * Owl.js JavaScript Library
 *
 * Copyright 2012, CodeParticle Inc
 * All Rights Reserved.
 */
(function($$, $) {

    // originally https://github.com/ded/klass
    (function() {

        var klass = function () {
            var context = this
                , old = context.klass
                , f = 'function'
                , fnTest = /xyz/.test(function () {xyz}) ? /\bsupr\b/ : /.*/
                , proto = 'prototype'

            function klass(func) {
                return extend.call(isFn(func) ? func : function () {}, func, true)
            }

            function isFn(o) {
                return typeof o === f
            }

            function wrap(k, fn, supr) {
                return function () {
                    var tmp = this.supr
                    this.supr = supr[proto][k]
                    var ret = fn.apply(this, arguments)
                    this.supr = tmp
                    return ret
                }
            }

            function process(what, o, supr) {
                for (var k in o) {
                    if (o.hasOwnProperty(k)) {
                        what[k] = isFn(o[k])
                            && isFn(supr[proto][k])
                            && fnTest.test(o[k])
                            ? wrap(k, o[k], supr) : o[k]
                    }
                }
            }

            function extend(constructor, fromSub, def) {
				if (typeof constructor != 'function') {
					def = constructor;
					fromSub = true;
				}
				else if (typeof fromSub != 'boolean') {
					def = fromSub;
					fromSub = false;
				}
                // must redefine noop each time so it doesn't inherit from previous arbitrary classes
                function noop() {}
                noop[proto] = this[proto]
                var supr = this
                    , prototype = new noop()
                    , isFunction = isFn(constructor)
                    , _constructor = isFunction ? constructor : this
                function fn() {
					fromSub || isFunction && supr.apply(this, arguments)
					var tmp = this.supr;
					this.supr = supr;
					_constructor.apply(this, arguments);
					this.supr = tmp;
                }

				if (def) process(prototype, def, supr)
				prototype.constructor = fn
				fn[proto] = prototype

                fn.extend = arguments.callee
                fn.statics = function (o, optFn) {
                    o = typeof o == 'string' ? (function () {
                        var obj = {}
                        obj[o] = optFn
                        return obj
                    }()) : o
                    process(this, o, supr)
                    return this
                }

                return fn
            }

            return klass
        }

        // create Class object
        $$.Class = (new (klass())).extend({
            destroy : function() {
                for(prop in this)
                    this[prop] = null;
            }
        });

    })();

    $$.Event = $$.Class.extend(function() {
        this.__class_event = {
            event : {}
        }
    }, false, {
		bind : function(event, callback) {
			var data = this.__class_event;
			if (!data.event[event]) data.event[event] = [];
			data.event[event].push(callback);
			return this;
		},
		unbind : function(event, callback) {
			var data = this.__class_event;
			if (event) {
				if (callback) {
					var a = data.event[event];
					for(var i=0, j=a.length; i<j; i++) {
						if (a[i]==callback) {
							a.splice(i, 1);
							break;
						}
					}
				} else
					delete data.event[event];
			} else {
				delete data.event;
				data.event = {};
			}
			return this;
		},
		trigger : function(event) {
			var data = this.__class_event;
			if (data.event[event]) {
				var callbacks = data.event[event];
				for(var i=0,j=callbacks.length;i<j;i++) {
					if (arguments.length>1) {
						var args = Array.prototype.slice.call(arguments, 1);
						args.push(event);
						callbacks[i].apply(this, args);
					} else
						callbacks[i](event);
				}
			};
			return this;
		},
        destroy : function() {
            this.unbind();
            return this.supr();
        }
    });

    $$.Events = new $$.Event();

}) (Owl, jQuery);