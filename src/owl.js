/*!
 * Owl.js JavaScript Library
 *
 * Copyright 2012, CodeParticle Inc
 * All Rights Reserved.
 */

(function($) {

    var owlJs = 'owl.js';

    var owlFiles = [ // files to be dynamically included
        'owl.core.js',
        'owl.class.js',
		'owl.mvc.js'
    ];

    if (owlFiles.length > 0) {
        $('script').each(function() {
            var src = $(this).attr('src');
            var i;
            if (src && (i = src.indexOf(owlJs, this.length - owlJs.length)) > -1) {
                var path = src.substring(0, i);
                for (var j = 0, k = owlFiles.length; j < k; j++)
                    document.write('<script type="text/javascript" src="'+path+owlFiles[j]+'"></script>');
                return false;
            }
        });
    }

})(jQuery);